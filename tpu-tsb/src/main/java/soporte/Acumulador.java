package soporte;

public class Acumulador {
    int valor  = 0;


    public Acumulador(){}

    public void add(){
        valor ++;
    }

    public void acumular(int x){
        valor += x;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getValor() {
        return valor;
    }

}
