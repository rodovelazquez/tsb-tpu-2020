package soporte;

import interfaz.InterfazController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import model.Postulacion;
import model.Region;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Importador {

    private static ArrayList<Postulacion> postulantes;

    public static void setPostulantes(ArrayList<Postulacion> pos){
        Importador.postulantes = pos;
    }

    public static ArrayList<Postulacion> getPostulantes() {
        return postulantes;
    }

    private static Region argentina;

    public static Region getArgentina() {
        return argentina;
    }

    public static void setArgentina(Region arg) {
        Importador.argentina = arg;
    }

    public static Service importarPostulaciones(final InterfazController interfaz, final File file) {

        return new Service() {
            @Override
            protected Task createTask() {
                return new Task() {
                    @Override
                    protected Object call() throws Exception {
                        try (Scanner scanner = new Scanner(file)) {
                            ArrayList<Postulacion> pos = new ArrayList<Postulacion>();
                            scanner.nextLine(); // La primer linea son encabezados
                            while (scanner.hasNextLine()) {
                                String[] linea = scanner.nextLine().split("\\|");
                                String codCategoria = linea[0];
                                if (codCategoria.equals("000100000000000")) {
                                    String codAgrupacion = linea[2];
                                    String nomAgrupacion = linea[3];
                                    String codLista = linea[4];
                                    String nomLista = linea[5];
                                    Postulacion p = new Postulacion (codAgrupacion, nomAgrupacion, codLista, nomLista);
                                    pos.add(p);
                                }

                            }
                            setPostulantes(pos);
                        }
                        catch (Exception e){
                            System.out.println(e.getMessage());
                            interfaz.makeMessageBox(e.getMessage());
                        }
                        interfaz.setPostulacionesLoaded(true);
                        interfaz.getButtonImportarRegiones().setDisable(false);
                        interfaz.getButtonImportarPostulaciones().setDisable(true);
                        interfaz.getProgressBarPostulaciones().setProgress(1);
                        return null;
                    }
                };
            }
        };

    }

    public static Service importarRegiones(final InterfazController interfaz, final File file){

        return new Service() {
            @Override
            protected Task createTask() {
                return new Task() {
                    @Override
                    protected Object call() throws Exception {
                        try (Scanner scanner = new Scanner(file)) {
                            scanner.nextLine(); // La primer linea son encabezados
                            while (scanner.hasNextLine()) {
                                String codigoDistrito = "", codigoSeccion = "", codigoCircuito = "";
                                String[] linea = scanner.nextLine().split("\\|");
                                String codigoRegion = linea[0];
                                String nombreRegion = linea[1];

                                int cantNumeros = codigoRegion.length();
                                switch (cantNumeros) {
                                    case 2:
                                        codigoDistrito = codigoRegion;
                                        break;
                                    case 5:
                                        codigoDistrito = codigoRegion.substring (0, 2);
                                        codigoSeccion = codigoRegion.substring (2, 5);
                                        break;
                                    case 11:
                                        codigoDistrito = codigoRegion.substring (0, 2);
                                        codigoSeccion = codigoRegion.substring (2, 5);
                                        codigoCircuito = codigoRegion.substring (5, 11);
                                        break;
                                    case 8:
                                }
                                Region distrito = null, seccion = null;
                                if (!codigoDistrito.isEmpty()) {
                                    if (!argentina.existeSubRegion(codigoDistrito)) {
                                        argentina.agregarSubRegion(codigoDistrito, new Region(nombreRegion));
                                    }
                                    distrito = argentina.obtenerSubRegion(codigoDistrito);
                                    if(cantNumeros == 2) {
                                        distrito.setNombre(nombreRegion);
                                    }
                                }
                                if (!codigoSeccion.isEmpty()) {
                                    if (!distrito.existeSubRegion(codigoSeccion)) {
                                        distrito.agregarSubRegion(codigoSeccion, new Region(nombreRegion));
                                    }
                                    seccion = distrito.obtenerSubRegion(codigoSeccion);
                                    if(cantNumeros == 5) {
                                        seccion.setNombre(nombreRegion);
                                    }
                                }
                                if (!codigoCircuito.isEmpty()) {
                                    if (!seccion.existeSubRegion(codigoCircuito)) {
                                        seccion.agregarSubRegion(codigoCircuito, new Region(nombreRegion));
                                    }
                                }
                            }
                        }
                        catch (Exception e){
                            System.out.println(e.getMessage());
                            interfaz.makeMessageBox(e.getMessage());
                        }
                        interfaz.setRegionesLoaded(true);
                        interfaz.getButtonCalcularTotalesMesas().setDisable(false);
                        interfaz.getButtonImportarRegiones().setDisable(true);
                        interfaz.getProgressBarRegiones().setProgress(1);
                        return null;
                    }
                };
            }
        };
    }

    public static Service calcularMesas(final InterfazController interfaz, final File file) {

        return new Service() {
            @Override
            protected Task createTask() {
                return new Task() {
                    @Override
                    protected Object call() throws Exception {
                        try (Scanner scanner = new Scanner(file)) {
                            scanner.nextLine(); // La primer linea son encabezados
                            interfaz.getProgressBarTotalesMesas().setProgress(0);
                            int contador = 0;
                            int totalLineas = 3302596;
                            while (scanner.hasNextLine()) {
                                String[] linea = scanner.nextLine ().split ("\\|");
                                contador++;

                                String codigoCategoria = (linea[4]);
                                if (codigoCategoria.equals ("000100000000000")) {
                                    String codigoCircuito = (linea[2]);
                                    String codigoMesa = (linea[3]);
                                    String codigoAgrupacion = (linea[5]);
                                    String votosAgrupacion = (linea[6]);

                                    String codDistrito = "", codSeccion = "", codCircuito = "";
                                    codDistrito = codigoCircuito.substring(0, 2);
                                    codSeccion = codigoCircuito.substring(2, 5);
                                    codCircuito = codigoCircuito.substring(5, 11);

                                    int lista = Integer.parseInt(codigoAgrupacion);
                                    int votos = Integer.parseInt(votosAgrupacion);

                                    argentina.sumarVotos(lista, Integer.parseInt(votosAgrupacion));
                                    argentina.obtenerSubRegion(codDistrito).sumarVotos(lista, votos);
                                    argentina.obtenerSubRegion(codDistrito).obtenerSubRegion(codSeccion).
                                            sumarVotos(lista, votos);
                                    argentina.obtenerSubRegion(codDistrito).obtenerSubRegion(codSeccion).
                                            obtenerSubRegion(codCircuito).sumarVotos(lista, votos);

                                    Region seccion = argentina.obtenerSubRegion(codDistrito).
                                            obtenerSubRegion(codSeccion).obtenerSubRegion(codCircuito);
                                    if (!seccion.existeSubRegion(codigoMesa)) {
                                        seccion.agregarSubRegion(codigoMesa, new Region(codigoMesa));
                                    }

                                    argentina.obtenerSubRegion(codDistrito).obtenerSubRegion(codSeccion).
                                            obtenerSubRegion(codCircuito).obtenerSubRegion(codigoMesa).
                                            sumarVotos(lista, votos);
                                }
                                if (contador == 5000) {
                                    double progreso = interfaz.getProgressBarTotalesMesas().getProgress() + 0.001513;
                                    interfaz.getProgressBarTotalesMesas().setProgress(progreso);
                                    contador = 0;
                                }

                            }

                        }
                        catch (Exception e){
                            System.out.println(e.getMessage());
                            interfaz.makeMessageBox(e.getMessage());
                        }
                        interfaz.setTotalesMesasLoaded(true);
                        interfaz.getButtonCalcularTotalesMesas().setDisable(true);
                        interfaz.verificarHabilitarButtonContinuar();
                        interfaz.getProgressBarPostulaciones().setProgress(1);
                        return null;
                    }
                };
            }
        };
    }

    public static ObservableList<Region> getDistritos(Region r){
        ArrayList<Region> nomDistritos = new ArrayList<> ();
        nomDistritos.addAll(r.getSubRegion().values());
        ObservableList<Region> options = FXCollections.observableArrayList(nomDistritos);
        return options;
    }

}
