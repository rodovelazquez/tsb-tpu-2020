package model;

import soporte.Importador;

import java.util.ArrayList;

public class Resultado {

    private Integer codigo;
    private String nombre;
    private Integer votos;
    private ArrayList<Postulacion> postulantes;

    public Resultado(int codigoAgrup) {
        this.postulantes = Importador.getPostulantes();
        this.codigo = codigoAgrup;

        for (Object pos:
             postulantes) {
            Postulacion postulacion = (Postulacion) pos;
            if (postulacion.getCodigoAgrupacion().equals(String.valueOf(codigoAgrup))) {
                nombre = postulacion.getNombreAgrupacion();
            }
        }
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getVotos() {
        return votos;
    }

    public void setVotos(Integer votos) {
        this.votos = votos;
    }

    public ArrayList<Postulacion> getPostulantes() {
        return postulantes;
    }

    public void setPostulantes(ArrayList<Postulacion> postulantes) {
        this.postulantes = postulantes;
    }

    @Override
    public String toString() {
        return "Resultado: [ " + "codigo = " + codigo + ", nombre = '" + nombre + '\'' + ", votos = " + votos + " ]";
    }

}
