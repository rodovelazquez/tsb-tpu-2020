package model;

import soporte.Acumulador;
import soporte.Importador;
import soporte.TSBHashtableDA;

import java.util.ArrayList;

public class Region {

    TSBHashtableDA<Integer, Acumulador> totales;
    TSBHashtableDA<String, Region> subRegion;
    String nombre;

    public TSBHashtableDA<String, Region> getSubRegion() {
        return subRegion;
    }

    public void setSubRegion(TSBHashtableDA<String, Region> subRegion) {
        this.subRegion = subRegion;
    }

    public Region(String nombre) {
        this.nombre = nombre;
        totales = new TSBHashtableDA<> ();
        this.cargarCodigoAgrupacion();
        subRegion = new TSBHashtableDA<>();
    }

    private void cargarCodigoAgrupacion() {

        ArrayList postulantes = Importador.getPostulantes();

        for(Object x: postulantes){
            Postulacion p = (Postulacion) x;
            this.totales.put(Integer.valueOf (p.getCodigoAgrupacion()), new Acumulador ());
        }
    }


    public void agregarSubRegion(String k, Region c) {
        subRegion.put (k, c);
    }

    public void setNombre(String nom){this.nombre = nom;}

    public String getNombre(){return nombre;}

    public Region obtenerSubRegion(String k) { return subRegion.get (k); }


    public void sumarVotos(Integer codAgrupacion, int cantVotos){
        totales.get(codAgrupacion).acumular (cantVotos);
    }

    public Integer getVotos(int lista) {
        return totales.get(lista).getValor();
    }

    public TSBHashtableDA getTotales(){return totales;}


    public boolean existeSubRegion(String k){
        return subRegion.containsKey (k);
    }

    @Override
    public String toString() {
        return  nombre;
    }
}
