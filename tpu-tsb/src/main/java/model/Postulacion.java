package model;

public class Postulacion {

    private String codigoAgrupacion;
    private String nombreAgrupacion;
    private String codigoLista;
    private String nombreLista;

    public Postulacion(String codAgrupacion, String nomAgrupacion, String codLista, String nomLista) {
        this.codigoAgrupacion = codAgrupacion;
        this.nombreAgrupacion = nomAgrupacion;
        this.codigoLista = codLista;
        this.nombreLista = nomLista;
    }

    public String getCodigoAgrupacion() {
        return codigoAgrupacion;
    }

    public void setCodigoAgrupacion(String codigoAgrupacion) {
        this.codigoAgrupacion = codigoAgrupacion;
    }

    public String getNombreAgrupacion() {
        return nombreAgrupacion;
    }

    public void setNombreAgrupacion(String nombreAgrupacion) {
        this.nombreAgrupacion = nombreAgrupacion;
    }

    public String getCodigoLista() {
        return codigoLista;
    }

    public void setCodigoLista(String codigoLista) {
        this.codigoLista = codigoLista;
    }

    public String getNombreLista() {
        return nombreLista;
    }

    public void setNombreLista(String nombreLista) {
        this.nombreLista = nombreLista;
    }

}
