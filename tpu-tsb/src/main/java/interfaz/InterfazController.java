package interfaz;

import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import model.Region;
import model.Resultado;
import soporte.Importador;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;
// javafx.event.ActionEvent actionEvent

public class InterfazController implements Initializable {

    @FXML
    private ProgressBar progressBarPostulaciones;

    @FXML
    private ProgressBar progressBarRegiones;

    @FXML
    private ProgressBar progressBarTotalesMesas;

    public ProgressBar getProgressBarPostulaciones() {
        return progressBarPostulaciones;
    }

    public ProgressBar getProgressBarRegiones() {
        return progressBarRegiones;
    }

    public ProgressBar getProgressBarTotalesMesas() {
        return progressBarTotalesMesas;
    }

    @FXML
    private Button buttonImportarPostulaciones;

    @FXML
    private Button buttonImportarRegiones;

    @FXML
    private Button buttonCalcularTotalesMesas;

    public Button getButtonImportarPostulaciones() {
        return buttonImportarPostulaciones;
    }

    public Button getButtonImportarRegiones() {
        return buttonImportarRegiones;
    }

    public Button getButtonCalcularTotalesMesas() {
        return buttonCalcularTotalesMesas;
    }

    @FXML
    private Button buttonContinuar;

    @FXML
    private Button buttonLimpiarFiltros;

    @FXML
    private Button buttonResultadosTotales;

    @FXML
    private Button buttonResultadosFiltros;

    @FXML
    private ComboBox<Region> comboBoxDistrito;

    @FXML
    private ComboBox<Region> comboBoxSeccion;

    @FXML
    private ComboBox<Region> comboBoxCircuito;

    @FXML
    private ComboBox<Region> comboBoxMesa;

    @FXML
    private TableView<Resultado> tableResultados;

    private boolean isPostulacionesLoaded;
    private boolean isRegionesLoaded;
    private boolean isTotalesMesasLoaded;

    private Region distrito;
    private Region seccion;
    private Region circuito;
    private Region mesa;

    public void setPostulacionesLoaded(boolean postulacionesLoaded) {
        isPostulacionesLoaded = postulacionesLoaded;
    }

    public void setRegionesLoaded(boolean regionesLoaded) {
        isRegionesLoaded = regionesLoaded;
    }

    public void setTotalesMesasLoaded(boolean totalesMesasLoaded) {
        isTotalesMesasLoaded = totalesMesasLoaded;
    }

    public InterfazController(){

    }

    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        this.isPostulacionesLoaded = false;
        this.isRegionesLoaded = false;
        this.isTotalesMesasLoaded = false;

        distrito = seccion = circuito = mesa = null;
        this.blockControls();

        this.iniciarTabla();
    }

    private void iniciarTabla() {
        this.tableResultados.getColumns().clear();
        TableColumn<Resultado, Integer> columnaId = new TableColumn<>("Id");
        columnaId.setPrefWidth(86.0);
        TableColumn<Resultado, String> columnaNombre = new TableColumn<>("Nombre Partido");
        columnaNombre.setPrefWidth(328.0);
        TableColumn<Resultado, Integer> columnaVotos = new TableColumn<>("Cantidad Votos");
        columnaVotos.setPrefWidth(115.0);

        columnaId.setCellValueFactory(new PropertyValueFactory<Resultado, Integer>("codigo"));
        columnaNombre.setCellValueFactory(new PropertyValueFactory<Resultado, String>("nombre"));
        columnaVotos.setCellValueFactory(new PropertyValueFactory<Resultado, Integer>("votos"));
        this.tableResultados.getColumns().setAll(columnaId, columnaNombre, columnaVotos);
    }

    public void makeMessageBox(String error) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("Error");
        dialog.setHeaderText("Error durante la ejecución");
        dialog.setContentText(error);
        dialog.showAndWait();
    }

    private void blockControls(){
        this.buttonImportarPostulaciones.setDisable(false);
        this.buttonImportarRegiones.setDisable(true);
        this.buttonCalcularTotalesMesas.setDisable(true);
        this.buttonContinuar.setDisable(true);

        this.comboBoxDistrito.setDisable(true);
        this.comboBoxSeccion.setDisable(true);
        this.comboBoxCircuito.setDisable(true);
        this.comboBoxMesa.setDisable(true);

        this.buttonLimpiarFiltros.setDisable(true);
        this.buttonResultadosFiltros.setDisable(true);
        this.buttonResultadosTotales.setDisable(true);
        this.tableResultados.setDisable(true);
    }

    public void verificarHabilitarButtonContinuar() {
        if (this.isTotalesMesasLoaded && this.isRegionesLoaded && this.isPostulacionesLoaded){
            this.buttonContinuar.setDisable(false);
        }
        else {
            this.makeMessageBox("No se ha cargado uno de los archivos");
        }
    }

    private void llenarTabla(Region reg) {
        this.tableResultados.getItems().clear();
        Set set =reg.getTotales().keySet();
        Integer[] listas = new Integer[set.size()];
        set.toArray(listas);

        for (int i = 0; i < listas.length; i++) {
            int lista = listas[i];
            Resultado res = new Resultado(lista);
            res.setVotos(reg.getVotos(lista));
            this.tableResultados.getItems().add(res);
        }
    }

    public void importarPostulacionesClick(ActionEvent actionEvent) {
        // File file = new File ("./descripcion_postulaciones.dsv");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("."));
        FileChooser.ExtensionFilter filter = new
                FileChooser.ExtensionFilter("Archivos .dsv", "*.dsv");
        fileChooser.getExtensionFilters().add(filter);
        fileChooser.setTitle("Seleccione el Archivo");
        File file = fileChooser.showOpenDialog(null);

        //Service service = Main.getPostulacionesService(this, file);
        Service service = Importador.importarPostulaciones(this, file);
        // progressBarPostulaciones.progressProperty().bind(service.progressProperty());
        service.start();
    }

    public void importarRegionesClick(ActionEvent actionEvent) {
        // File file = new File ("./descripcion_regiones.dsv");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("."));
        FileChooser.ExtensionFilter filter = new
                FileChooser.ExtensionFilter("Archivos .dsv", "*.dsv");
        fileChooser.getExtensionFilters().add(filter);
        fileChooser.setTitle("Seleccione el Archivo");
        File file = fileChooser.showOpenDialog(null);

        Service service = Importador.importarRegiones(this, file);
        Importador.setArgentina(new Region("Argentina"));
        service.start();
    }

    public void calcularTotalesMesasClick(ActionEvent actionEvent) {
        // Iteraciones esperadas para la ProgressBar: 3302596
        // File file = new File ("./mesas_totales_agrp_politica.dsv");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("."));
        FileChooser.ExtensionFilter filter = new
                FileChooser.ExtensionFilter("Archivos .dsv", "*.dsv");
        fileChooser.getExtensionFilters().add(filter);
        fileChooser.setTitle("Seleccione el Archivo");
        File file = fileChooser.showOpenDialog(null);

        Service service = Importador.calcularMesas(this, file);
        service.start();
    }

    public void buttonContinuarClick(ActionEvent actionEvent) {
        this.buttonResultadosTotales.setDisable(false);
        this.buttonLimpiarFiltros.setDisable(false);
        this.comboBoxDistrito.setDisable(false);

        this.cargarCombo(this.comboBoxDistrito, Importador.getArgentina());
        this.tableResultados.setDisable(false);
        this.buttonContinuar.setDisable(true);
    }

    private void cargarCombo(ComboBox<Region> combo, Region distrito) {
        ObservableList<Region> obs = Importador.getDistritos(distrito);
        combo.setItems(obs);
    }

    public void buttonLimpiarFiltrosClick(ActionEvent actionEvent) {
        this.tableResultados.getItems().clear();

        this.comboBoxDistrito.setValue(null);
        this.comboBoxSeccion.setValue(null);
        this.comboBoxCircuito.setValue(null);
        this.comboBoxMesa.setValue(null);

        this.comboBoxSeccion.setDisable(true);
        this.comboBoxCircuito.setDisable(true);
        this.comboBoxMesa.setDisable(true);
        this.buttonResultadosFiltros.setDisable(true);

        distrito = seccion = circuito = mesa = null;
        this.cargarCombo(this.comboBoxDistrito, Importador.getArgentina());
    }

    public void comboDistritoClick(ActionEvent actionEvent) {
        this.comboBoxSeccion.getItems().clear();
        this.comboBoxCircuito.getItems().clear();
        this.comboBoxCircuito.setDisable(true);
        this.comboBoxMesa.getItems().clear();
        this.comboBoxMesa.setDisable(true);

        if (this.comboBoxDistrito.getValue() != null) {
            this.distrito = this.comboBoxDistrito.getValue();
        }

        this.buttonResultadosFiltros.setDisable(false);
        this.comboBoxSeccion.setDisable(false);
        this.cargarCombo(this.comboBoxSeccion, distrito);
        seccion = circuito = mesa = null;
    }

    public void comboSeccionClick(ActionEvent actionEvent) {
        this.comboBoxCircuito.getItems().clear();
        this.comboBoxMesa.getItems().clear();
        this.comboBoxMesa.setDisable(true);

        if (this.comboBoxSeccion.getValue() != null) {
            this.seccion = this.comboBoxSeccion.getValue();
        }

        this.buttonResultadosFiltros.setDisable(false);
        this.comboBoxCircuito.setDisable(false);
        this.cargarCombo(comboBoxCircuito, seccion);
        circuito = mesa = null;
    }

    public void comboCircuitoClick(ActionEvent actionEvent) {
        this.comboBoxMesa.getItems().clear();

        if (this.comboBoxCircuito.getValue() != null) {
            this.circuito = this.comboBoxCircuito.getValue();
        }

        this.buttonResultadosFiltros.setDisable(false);
        this.comboBoxMesa.setDisable(false);
        this.cargarCombo(comboBoxMesa, circuito);
        mesa = null;
    }

    public void comboMesaClick(ActionEvent actionEvent) {
        if (this.comboBoxMesa.getValue() != null) {
            this.mesa = this.comboBoxMesa.getValue();
        }

        this.buttonResultadosFiltros.setDisable(false);
    }

    public void buttonResultadosTotalesClick(ActionEvent actionEvent) {
        this.buttonResultadosFiltros.setDisable(true);
        this.llenarTabla(Importador.getArgentina());
    }

    public void buttonResultadosFiltrosClick(ActionEvent actionEvent) {
        if (this.distrito != null) { this.llenarTabla(distrito); }
        if (this.seccion != null) { this.llenarTabla(seccion); }
        if (this.circuito != null) { this.llenarTabla(circuito); }
        if (this.mesa != null) { this.llenarTabla(mesa); }
    }
}
