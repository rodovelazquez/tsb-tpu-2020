# TSB - TPU - 2020

Este repositorio corresponde al TPU de la materia TSB cursada en el año 2020 en la UTN FRC

# Commit Message Convention

|Cabecera del commit		|Utilidad						|
|-------------------------------|-------------------------------------------------------|
|```Add:``` or ```Added:```	|Agregar archivos o código nuevo al proyecto		|
|```Docs:```			|Agregar o modificar archivos de documentación		|
|```Edit:``` or ```Edited:```	|Indicar que se estuvo editando cierto archivo		|
|```Feature:```			|Funcionalidad terminada				|
|```Fix:```			|Arreglo de bugs o errores				|
|```HotFix:```			|Arreglos menores y estéticos				|
|```Merge```			|Marcar el merge commit entre 2 branches		|
|```NoChange:```		|Sin cambios de funcionalidad, limpieza de código	|
|```Release:```			|Indica una release					|
|```Refactor:```		|Cambio de nombres de arhivos u objetos			|
|```Style:```			|Cambios estéticos, de interfaz, etc			|

> Nótese que ```'Merge'``` es la única que va sin ```':'``` al final

# JDK 8

Para poder usar JavaFX (el Windows Forms de java) es necesario usar el JDK 8. Yo voy a crear el proyecto con uno que ya tenía descargado en mi PC, pero se los dejo [en este link](https://drive.google.com/drive/folders/14fNsP7AQQvUsM41_j8Ot_Xtp8DwDsl8-?usp=sharing).

Para agregarlo a IntelliJ IDEA, hay que ir a ```File > New > Project...```, y en el combo que dice ```Project SDK``` hacen click en la opción ```Add JDK...``` y seleccionan la ruta en la que se lo descargaron.
